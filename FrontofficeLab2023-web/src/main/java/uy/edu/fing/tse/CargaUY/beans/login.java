package uy.edu.fing.tse.CargaUY.beans;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Named("logingub")
@SessionScoped
public class login implements Serializable {
    @ConfigProperty(name = "clientId")
    private String clientId;

    @ConfigProperty(name = "clientSecret")
    private String clientSecret;

    @ConfigProperty(name = "redirectUri")
    private String redirectUri;

    @ConfigProperty(name = "authorizationUrl")
    private String authorizationUrl;

    @ConfigProperty(name = "tokenUrl")
    private String tokenUrl;

    @ConfigProperty(name = "scope")
    private String scope;

    private String code;
    private String accessToken;
    private String idToken;

    @PostConstruct
    public void init() {
        clientId = "890192"; // Reemplaza con el ID de cliente correspondiente
        clientSecret = "457d52f181bf11804a3365b49ae4d29a2e03bbabe74997a2f510b179"; // Reemplaza con el secreto de cliente correspondiente
        redirectUri = "https://cargauy.web.elasticloud.uy/LaboratorioCargaUYgrupo12-web/rest/inicioSesion/callback"; // Reemplaza con la URL de redireccionamiento correcta
        authorizationUrl = "https://auth-testing.iduruguay.gub.uy/oidc/v1/authorize";
        tokenUrl = "https://auth-testing.iduruguay.gub.uy/oidc/v1/token";
        scope = "openid document personal_info auth_info";    }

    public String login() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

        StringBuilder authorizationUrlBuilder = new StringBuilder(authorizationUrl);
        authorizationUrlBuilder.append("?response_type=code");
        authorizationUrlBuilder.append("&client_id=").append(clientId);
        authorizationUrlBuilder.append("&redirect_uri=").append(redirectUri);
        authorizationUrlBuilder.append("&scope=").append(scope);

        try {
            externalContext.redirect(authorizationUrlBuilder.toString());
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IOException e) {
            e.printStackTrace();
            // Aquí puedes realizar alguna acción adicional, como mostrar un mensaje de error en la página
            return null;
        }

        return "index.xhtml"; // Cambia "login.xhtml" por la página a la que deseas redirigir después de la autenticación exitosa
    }
    public void testGub() throws Exception {
        String url = "https://auth-testing.iduruguay.gub.uy/oidc/v1/token";
        /*String clientId = "123456789";
        String clientSecret = "0Pg8RaabLluvuoG3";
        String authorization = Base64.encode(clientId + ":" + clientSecret);*/
        String authorization ="ODkwMTkyOjQ1N2Q1MmYxODFiZjExODA0YTMzNjViNDlhZTRkMjlhMmUwM2JiYWJlNzQ5OTdhMmY1MTBiMTc5";
        // Construir los parámetros del cuerpo de la solicitud
        String grantType = "authorization_code";
        String redirectUri = "https://cargauy.web.elasticloud.uy/LaboratorioCargaUYgrupo12-web/rest/inicioSesion/callback/";
        String requestBody = "grant_type=" + encodeValue(grantType)
                + "&code=" + encodeValue(code)
                + "&redirect_uri=" + encodeValue(redirectUri);

        // Configurar los encabezados de la solicitud
        HttpClient httpClient = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(url))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Authorization", "Basic " + authorization)
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        // Realizar la solicitud y obtener la respuesta
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        // Obtener los resultados
        int statusCode = response.statusCode();
        String responseBody = response.body();
        HttpHeaders headers = response.headers();

        // Imprimir los resultados
        System.out.println("Status Code: " + statusCode);
        System.out.println("Response Body: " + responseBody);
        System.out.println("Headers: " + headers.map());
    }

    // Función de ayuda para codificar los valores de los parámetros
    private static String encodeValue(String value) throws UnsupportedEncodingException {
        return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
    }
    public String callbackHandling() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        code = request.getParameter("code");

        return "code.xhtml";
    }

    public String requestAccessToken() {
        try {
            Client client = ClientBuilder.newClient();
            WebTarget authTarget = client.target(tokenUrl);

            String authorizationHeader = clientId + ":" + clientSecret;
            String authorizationHeaderValue = "Basic " + Base64.getEncoder().encodeToString(authorizationHeader.getBytes());

            String requestBody = "code=" + code + "&grant_type=authorization_code&redirect_uri=" + redirectUri;

            WebTarget tokenTarget = client.target(tokenUrl);
            String response = tokenTarget.request(MediaType.APPLICATION_FORM_URLENCODED)
                    .header("Authorization", authorizationHeaderValue)
                    .post(Entity.entity(requestBody, MediaType.APPLICATION_FORM_URLENCODED), String.class);

            JSONObject jsonResponse = new JSONObject(response);
            accessToken = jsonResponse.getString("access_token");
            idToken = jsonResponse.getString("id_token");

            return "tokens.xhtml";
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // Getters and Setters

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getAuthorizationUrl() {
        return authorizationUrl;
    }

    public void setAuthorizationUrl(String authorizationUrl) {
        this.authorizationUrl = authorizationUrl;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }
}
