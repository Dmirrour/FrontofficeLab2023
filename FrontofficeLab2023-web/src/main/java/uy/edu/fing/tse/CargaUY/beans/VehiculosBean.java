package uy.edu.fing.tse.CargaUY.beans;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.primefaces.event.RowEditEvent;
import uy.edu.fing.tse.CargaUY.dto.EmpresaDTO;
import uy.edu.fing.tse.CargaUY.dto.GuiaDTO;
import uy.edu.fing.tse.CargaUY.dto.VehiculoDTO;
import uy.edu.fing.tse.CargaUY.entity.Empresa;
import uy.edu.fing.tse.CargaUY.entity.ITV;
import uy.edu.fing.tse.CargaUY.entity.PNC;
import uy.edu.fing.tse.CargaUY.entity.Vehiculo;
import uy.edu.fing.tse.CargaUY.model.Empresas;
import uy.edu.fing.tse.CargaUY.model.Vehiculos;
import uy.edu.fing.tse.CargaUY.response.RestResponse;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Named("vehiculosBean")
@SessionScoped
public class VehiculosBean implements Serializable {

    private Vehiculos vehiculos;
    private ArrayList<VehiculoDTO> list;
    private ArrayList<EmpresaDTO> listaEmpresas;
    private Empresas empresas; //private Empresa empresa;

    private Empresa empresa;

    private Long nroEmpresa;
    private String matricula;
    private String marca;
    private String modelo;
    private Long peso;
    private Long capacidadCarga;
    private PNC pnc;
    private ITV itv;
    private String filtro;
    private boolean estado;

    public VehiculosBean(){}
    @Inject
    private Sesion s;

    public void initLista(){
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/empresas/listar");
        empresas = target.request(MediaType.APPLICATION_JSON).get(Empresas.class);
        listaEmpresas = empresas.getListaEmpresas();

    }

    public void initVehiculosVE(){
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/vehiculos/listarVE/".concat(String.valueOf(s.getAsignada().getNroEmpresa())));
        vehiculos = target.request(MediaType.APPLICATION_JSON).get(Vehiculos.class);
        list = vehiculos.getListaVehiculos();
        System.out.println(list);
    }
    public void initVehiculosVED(){
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/vehiculos/listarVED/".concat(String.valueOf(s.getAsignada().getNroEmpresa())));
        vehiculos = target.request(MediaType.APPLICATION_JSON).get(Vehiculos.class);
        list = vehiculos.getListaVehiculos();
    }

    public void addVehiculo(){
        Vehiculo nuevoVehiculo = Vehiculo.builder()
                .matricula(matricula)
                .marca(marca)
                .modelo(modelo)
                .peso(peso)
                .estado(true)
                .capacidadCarga(capacidadCarga)
                .build();
        System.out.println(nuevoVehiculo);
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/vehiculos/agregar/".concat(String.valueOf(s.getAsignada().getNroEmpresa())));
        Response response = target.request(MediaType.APPLICATION_JSON).post(Entity.entity(nuevoVehiculo, MediaType.APPLICATION_JSON));

        RestResponse result = response.readEntity(RestResponse.class);
        if(result.getStatus() == 201)
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, result.getMsg(), ""));
        else
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, result.getMsg(), ""));
    }

    public void eliminarVehiculo(String matricula){
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/vehiculos/eliminar/".concat(String.valueOf(matricula)));
        boolean exito = target.request().delete(Boolean.class);
        if(exito){
            initLista();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Se ha borrado el Vehiculo", ""));
        }else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo borrar el Vehiculo", ""));
        }
    }

    public void modificarVehiculo(RowEditEvent event){
        VehiculoDTO v=(VehiculoDTO) event.getObject();
        System.out.println(marca);
        System.out.println(modelo);
        if(marca.isEmpty()&&marca=="")//no se porque no entra
            marca=v.getMarca();
        if(modelo.isEmpty()&&modelo=="")
            modelo=v.getModelo();
        if(peso==null)
            peso=v.getPeso();
        if(capacidadCarga==null)
            capacidadCarga=v.getCapacidadCarga();
        Vehiculo vehiculoModificado = Vehiculo.builder()
                .matricula(v.getMatricula())
                .marca(marca)
                .modelo(modelo)
                .peso(peso)
                .capacidadCarga(capacidadCarga)
                .estado(v.isEstado())
                .build();

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/vehiculos/modificar");
        target.request(MediaType.APPLICATION_JSON).put(Entity.entity(vehiculoModificado, MediaType.APPLICATION_JSON));
        initVehiculosVE();
        marca="";
        modelo="";
        peso=null;
        capacidadCarga=null;
    }
    public void cancelar(RowEditEvent event) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cancelado", ""));
    }


    public String determinarEstado(Date fechaPermiso) {
        Date fechaActual = new Date();
        if(fechaPermiso != null){
            if ( fechaPermiso.before(fechaActual)) {
                return "Vencido";
            } else {
                return "Vigente";
            }
        }else{
            return "";
        }

    }
    public String determinarEstadoColor(Date fechaPermiso) {
        Date fechaActual = new Date();
        if (fechaPermiso.before(fechaActual)) {
            return "red";
        } else {
            return "green";
        }
    }


    public Vehiculos getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(Vehiculos vehiculos) {
        this.vehiculos = vehiculos;
    }

    public ArrayList<VehiculoDTO> getList() {
        return list;
    }

    public void setList(ArrayList<VehiculoDTO> list) {
        this.list = list;
    }

    public ArrayList<EmpresaDTO> getListaEmpresas() {
        return listaEmpresas;
    }

    public void setListaEmpresas(ArrayList<EmpresaDTO> listaEmpresas) {
        this.listaEmpresas = listaEmpresas;
    }

    public Empresas getEmpresas() {
        return empresas;
    }

    public void setEmpresas(Empresas empresas) {
        this.empresas = empresas;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Long getNroEmpresa() {
        return nroEmpresa;
    }

    public void setNroEmpresa(Long nroEmpresa) {
        this.nroEmpresa = nroEmpresa;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Long getPeso() {
        return peso;
    }

    public void setPeso(Long peso) {
        this.peso = peso;
    }

    public Long getCapacidadCarga() {
        return capacidadCarga;
    }

    public void setCapacidadCarga(Long capacidadCarga) {
        this.capacidadCarga = capacidadCarga;
    }

    public PNC getPnc() {
        return pnc;
    }

    public void setPnc(PNC pnc) {
        this.pnc = pnc;
    }

    public ITV getItv() {
        return itv;
    }

    public void setItv(ITV itv) {
        this.itv = itv;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
