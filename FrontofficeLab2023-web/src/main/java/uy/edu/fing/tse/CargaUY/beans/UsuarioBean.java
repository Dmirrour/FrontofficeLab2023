package uy.edu.fing.tse.CargaUY.beans;

import jakarta.annotation.ManagedBean;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import uy.edu.fing.tse.CargaUY.dto.UsuarioDTO;
import uy.edu.fing.tse.CargaUY.entity.Usuario;
import uy.edu.fing.tse.CargaUY.model.Usuarios;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named("usuariosBean")
@SessionScoped
public class UsuarioBean implements Serializable {
    private Usuarios usuarios;
    private ArrayList<UsuarioDTO> list;

    @Inject
    private Sesion s;

    public void initListaG(){//General Ciudadano
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/usuarios/listar");
        usuarios = target.request(MediaType.APPLICATION_JSON).get(Usuarios.class);
        list = usuarios.getListaCiudadanos();
    }
    public void initLista(){//choferes
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/usuarios/listarE/".concat(String.valueOf(s.getAsignada().getNroEmpresa())));
        usuarios = target.request(MediaType.APPLICATION_JSON).get(Usuarios.class);
        list = usuarios.getListaUsuarios();
    }
    public Usuario selectUsu() {
        //Integer ci = (Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("ci");
        Usuario user=(Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
        System.out.println(user);
        //UsuarioDTO usu = service.obtenerUsu(ci);
        return user;
    }

    public ArrayList<UsuarioDTO> getList() {
        return list;
    }

    public void setList(ArrayList<UsuarioDTO> list) {
        this.list = list;
    }
}
