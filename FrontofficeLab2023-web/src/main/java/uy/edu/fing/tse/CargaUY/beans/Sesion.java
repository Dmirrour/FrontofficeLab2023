package uy.edu.fing.tse.CargaUY.beans;

import jakarta.annotation.ManagedBean;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Named;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import uy.edu.fing.tse.CargaUY.entity.Empresa;
import uy.edu.fing.tse.CargaUY.entity.Login;
import uy.edu.fing.tse.CargaUY.entity.Usuario;

import java.io.Serializable;

@Named("login")
@ManagedBean
@SessionScoped
public class Sesion implements Serializable {
    private boolean isLoggedIn;
    private String pass;
    private Integer ci;
    private String tipoU= "no";
    private Empresa asignada;
    public String iniciarSesion(){
        Login login = Login.builder()
                .ci(ci)
                .pass(pass)
                .build();


        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/incioSaux/login");
        Usuario autenticado = target.request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(login, MediaType.APPLICATION_JSON), Usuario.class);
        System.out.println(autenticado);
        if(autenticado==null){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de Inicio de Sesion", "Usuario o contraseña incorrectos."));
            return "ERROR";
        }else{
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", autenticado);
            System.out.println(autenticado);
            tipoU=((Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user")).getTipoUsuario().name();
            asignada=((Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user")).getEmpresa();
            System.out.println(tipoU);

        }


        isLoggedIn = true;
        return "index.xhtml?faces-redirect=true";

    }
    public void verificarSesion(String donde) {
        try {
            FacesContext FC = FacesContext.getCurrentInstance();
            Usuario u = (Usuario) FC.getExternalContext().getSessionMap().get("user");
            if (u == null) {
                FC.getExternalContext().redirect("/FrontofficeLab2023-web/loginUsuario.xhtml");
            }else{
                switch (donde){
                    case "Cualquiera":
                        if(u==null)
                            FC.getExternalContext().redirect("/FrontofficeLab2023-web/index.xhtml");
                        break;
                    case "Empleado":
                        if(u.getTipoUsuario().toString()!="EMPLEADO")
                            FC.getExternalContext().redirect("/FrontofficeLab2023-web/index.xhtml");
                        break;
                    case "Funcionario":
                        if(u.getTipoUsuario().toString()!="FUNCIONARIO")
                            FC.getExternalContext().redirect("/FrontofficeLab2023-web/index.xhtml");
                        break;
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public String cerrarSesion(){
        isLoggedIn = false;
        tipoU= "no";
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        return "index.xhtml?faces-redirect=true";
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Integer getCi() {
        return ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    public String getTipoU() {
        return tipoU;
    }

    public void setTipoU(String tipoU) {
        this.tipoU = tipoU;
    }

    public Empresa getAsignada() {
        return asignada;
    }

    public void setAsignada(Empresa asignada) {
        this.asignada = asignada;
    }
}
