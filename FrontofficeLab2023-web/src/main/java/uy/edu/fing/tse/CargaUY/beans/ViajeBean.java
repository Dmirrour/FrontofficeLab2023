package uy.edu.fing.tse.CargaUY.beans;

import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import uy.edu.fing.tse.CargaUY.dto.*;
import uy.edu.fing.tse.CargaUY.entity.EstadoViaje;
import uy.edu.fing.tse.CargaUY.model.Viajes;
import uy.edu.fing.tse.CargaUY.response.RestResponse;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named("viajesBean")
@SessionScoped
public class ViajeBean implements Serializable {
    private ArrayList<ViajeDTO> viajeDTOS;
    private Viajes viajes;
    private String nombre;
    private UsuarioDTO chofer;
    private GuiaDTO guia;
    private VehiculoDTO vehiculo;
    private  long idChof;
    private  long idG;
    private  String matricula;
    @Inject
    private Sesion s;

    public void initiVE(){ //falta hacerlo rest
        Client client1 = ClientBuilder.newClient();
        //WebTarget target = client.target("https://cargauy.web.elasticloud.uy/LaboratorioCargaUYgrupo12-web/rest/viajes/listarVE/");//direccion de la pagina web
        WebTarget target = client1.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/viajes/listarVE/")
                .queryParam("id",s.getAsignada().getNroEmpresa());
        viajes= target.request(MediaType.APPLICATION_JSON).get(Viajes.class);
        viajeDTOS=viajes.getListaViajes();
    }

    public void altaViaje(){
        ViajeDTO nuevoViaje= ViajeDTO.builder()
                .estadoViaje(EstadoViaje.valueOf("PENDIENTE"))
                .nombre(nombre)
                .empresa(EmpresaDTO.builder().nroEmpresa(s.getAsignada().getNroEmpresa()).build())
                .chofer(UsuarioDTO.builder().id(idChof).build())
                .vehiculo(VehiculoDTO.builder().matricula(matricula).build())
                .guia(GuiaDTO.builder().idGuia(idG).build())
                .build();
        Client client = ClientBuilder.newClient();
        //WebTarget target = client.target("https://cargauy.web.elasticloud.uy/LaboratorioCargaUYgrupo12-web/rest/viajes/altaV");//direccion de la pagina web
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/viajes/altaV");
        Response response =target.request(MediaType.APPLICATION_JSON).post(Entity.entity(nuevoViaje, MediaType.APPLICATION_JSON));
        RestResponse result = response.readEntity(RestResponse.class);
        if(result.getStatus() == 201)
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, result.getMsg(), ""));
        else
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, result.getMsg(), ""));

    }
    public void eliminarViaje(Long idViaje){
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/viajes/BorrarV/".concat(String.valueOf(idViaje)));
        boolean exito = target.request().delete(Boolean.class);
        if(exito){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Se ha borrado el viaje ", ""));
        }else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo borrar el viaje ", ""));
        }
    }


    //©copyright uso free para cualquiera que lo desee jajaj


    public ArrayList<ViajeDTO> getViajeDTOS() {
        return viajeDTOS;
    }

    public void setViajeDTOS(ArrayList<ViajeDTO> viajeDTOS) {
        this.viajeDTOS = viajeDTOS;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public UsuarioDTO getChofer() {
        return chofer;
    }

    public void setChofer(UsuarioDTO chofer) {
        this.chofer = chofer;
    }

    public GuiaDTO getGuia() {
        return guia;
    }

    public void setGuia(GuiaDTO guia) {
        this.guia = guia;
    }

    public VehiculoDTO getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(VehiculoDTO vehiculo) {
        this.vehiculo = vehiculo;
    }

    public long getIdChof() {
        return idChof;
    }

    public void setIdChof(long idChof) {
        this.idChof = idChof;
    }

    public long getIdG() {
        return idG;
    }

    public void setIdG(long idG) {
        this.idG = idG;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
