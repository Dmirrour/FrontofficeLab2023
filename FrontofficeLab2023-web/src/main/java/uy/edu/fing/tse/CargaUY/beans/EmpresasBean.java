package uy.edu.fing.tse.CargaUY.beans;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import uy.edu.fing.tse.CargaUY.dto.EmpresaDTO;
import uy.edu.fing.tse.CargaUY.dto.UsuarioDTO;
import uy.edu.fing.tse.CargaUY.dto.VehiculoDTO;
import uy.edu.fing.tse.CargaUY.entity.Empresa;
import uy.edu.fing.tse.CargaUY.entity.TipoUsuario;
import uy.edu.fing.tse.CargaUY.model.Empresas;
import uy.edu.fing.tse.CargaUY.response.RestResponse;

import java.io.Serializable;
import java.util.ArrayList;

@Named("empresasBean")
@SessionScoped
public class EmpresasBean implements Serializable {

    private ArrayList<EmpresaDTO> list= new ArrayList<>();
    private String razonSocial;
    private String nombrePublico;
    private String direccion;
    private String filtro;
    private Long nroEmpresa;
    private UsuarioDTO ciudadanoSeleccionado;
    private Empresas e;
    private TipoUsuario tipU;
    private ArrayList<TipoUsuario> tipoUA= new ArrayList<>();
    @Inject
    private Sesion s;
    @Inject
    private UsuarioBean user;

    public EmpresasBean() {}
    public void soloE(){
        list.clear();
        list.add(EmpresaDTO.builder()
                .nroEmpresa(s.getAsignada().getNroEmpresa())
                .razonSocial(s.getAsignada().getRazonSocial())
                .nombrePublico(s.getAsignada().getNombrePublico())
                .direccion(s.getAsignada().getDireccion())
                .vehiculos(s.getAsignada().getVehiculosDTO())
                .ciudadanos(s.getAsignada().getCiudadanosDTO())
                .build());
    }

    public void initLista(){
        //WebTarget target = client.target("https://cargauy.web.elasticloud.uy/LaboratorioCargaUYgrupo12-web/rest/empresas/listar");//direccion de la pagina web
        Client client1 = ClientBuilder.newClient();
        WebTarget target = client1.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/empresas/listar");
        e = target.request(MediaType.APPLICATION_JSON).get(Empresas.class);
        list=e.getListaEmpresas();
        tipoUA.clear();
        tipoUA.add(TipoUsuario.EMPLEADO);
        tipoUA.add(TipoUsuario.CHOFER);

    }

    public void eliminarEmpresa(Long nroEmpresa){

        Client client = ClientBuilder.newClient();
        //WebTarget target = client.target("https://cargauy.web.elasticloud.uy/LaboratorioCargaUYgrupo12-web/rest/empresas/eliminar/".concat(String.valueOf(nroEmpresa)));//direccion de la pagina web
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/empresas/eliminar/".concat(String.valueOf(nroEmpresa)));
        boolean exito = target.request().delete(Boolean.class);
        if(exito){
            initLista();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Se ha borrado la empresa: ", ""));
        }else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo borrar la empresa ", ""));
        }
    }

    public void modificarEmpresa(){
        Empresa empresaModificada = Empresa.builder()
                .nroEmpresa(nroEmpresa)
                .razonSocial(razonSocial)
                .nombrePublico(nombrePublico)
                .direccion(direccion)
                .build();
        Client client = ClientBuilder.newClient();
        //WebTarget target = client.target("https://cargauy.web.elasticloud.uy/LaboratorioCargaUYgrupo12-web/rest/guia/altaguia");//direccion de la pagina web
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/empresas/modificar");
        Response response =target.request(MediaType.APPLICATION_JSON).put(Entity.entity(empresaModificada, MediaType.APPLICATION_JSON));
        RestResponse result = response.readEntity(RestResponse.class);
        if(result.getStatus() == 201){
            s.getAsignada().setDireccion(direccion);
            s.getAsignada().setNombrePublico(nombrePublico);
            s.getAsignada().setRazonSocial(razonSocial);
            soloE();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, result.getMsg(), ""));
        }
        else
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, result.getMsg(), ""));

    }

    public void asignarCiudadano(RowEditEvent event){
        UsuarioDTO u=(UsuarioDTO) event.getObject();
        Client client = ClientBuilder.newClient();
        //WebTarget target = client.target("https://cargauy.web.elasticloud.uy/LaboratorioCargaUYgrupo12-web/rest/empresas/signar-ciudadano/".concat(String.valueOf(nroEmpresa)));//direccion de la pagina web
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/empresas/asignar-ciudadano/"+nroEmpresa+"/"+tipU);
        target.request(MediaType.APPLICATION_JSON).put(Entity.entity(u, MediaType.APPLICATION_JSON));
        initLista();
        user.initListaG();
    }

    public void desvincularCiudadano(Long idCiudadano){
        Client client = ClientBuilder.newClient();
        //WebTarget target = client.target("https://cargauy.web.elasticloud.uy/LaboratorioCargaUYgrupo12-web/rest/empresas/signar-ciudadano/".concat(String.valueOf(nroEmpresa)));//direccion de la pagina web
        WebTarget target = client.target("http://localhost:8080/LaboratorioCargaUYgrupo12-web/rest/empresas/desvincular-ciudadano/"
                + idCiudadano + "/" + nroEmpresa);
        /*.queryParam("id", idCiudadano)
                .queryParam("nroEmpresa", nroEmpresa);*/
        Response response = target.request().put(Entity.json(null));
    }

    public void cancelar(RowEditEvent event) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cancelado", ""));
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombrePublico() {
        return nombrePublico;
    }

    public void setNombrePublico(String nombrePublico) {
        this.nombrePublico = nombrePublico;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public ArrayList<EmpresaDTO> getList() {
        return list;
    }

    public void setList(ArrayList<EmpresaDTO> list) {
        this.list = list;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public Long getNroEmpresa() {
        return nroEmpresa;
    }
    public void setNroEmpresa(Long nroEmpresa) {
        this.nroEmpresa = nroEmpresa;
    }

    public UsuarioDTO getCiudadanoSeleccionado() {
        return ciudadanoSeleccionado;
    }

    public void setCiudadanoSeleccionado(UsuarioDTO ciudadanoSeleccionado) {
        this.ciudadanoSeleccionado = ciudadanoSeleccionado;
    }

    public TipoUsuario getTipU() {
        return tipU;
    }

    public void setTipU(TipoUsuario tipU) {
        this.tipU = tipU;
    }

    public ArrayList<TipoUsuario> getTipoUA() {
        return tipoUA;
    }

    public void setTipoUA(ArrayList<TipoUsuario> tipoUA) {
        this.tipoUA = tipoUA;
    }
}
